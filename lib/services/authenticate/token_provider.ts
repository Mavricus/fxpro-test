import axios, { AxiosResponse } from 'axios';
import * as url from 'url';
import { FxproError, CODES } from '../../errors';
import { ServerConfig } from '../../factory';

export interface AuthenticationRequest {
    username: string;
    password: string;
}

export interface AuthenticationResponse {
    authToken: string;
    expiresIn: string;
}

export interface TokenProviderService {
    get(authInfo: AuthenticationRequest): Promise<AuthenticationResponse>;
}

export class TokenProviderImpl implements TokenProviderService {
    private static readonly AUTH_PATH = '/v1/authenticate';
    private static readonly POSITIVE_STATUSES = [200, 201];

    constructor(private config: ServerConfig) {
    }

    async get(authInfo: AuthenticationRequest): Promise<AuthenticationResponse> {
        try {
            let url = this.buildRequestUrl();
            let response: AxiosResponse<AuthenticationResponse> = await axios.post(url, authInfo);

            if (!TokenProviderImpl.POSITIVE_STATUSES.includes(response.status)) {
                throw new FxproError(CODES.unauthorized, 'Cannot retrieve authentication token');
            }
            return response.data;
        } catch (e) {
            if (e.code != null) {
                throw e;
            }
            throw new FxproError(CODES.unauthorized, e.message);
        }
    }

    private buildRequestUrl(): string {
        let info: url.UrlObject = {
            protocol: this.config.protocol,
            hostname: this.config.hostname,
            port: this.config.port,
            pathname: TokenProviderImpl.AUTH_PATH
        };
        return url.format(info);
    }
}