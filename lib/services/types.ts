export interface BaseRequest {
    token: string;
}

export interface BaseRequestWithData<T> extends BaseRequest {
    data: T;
}