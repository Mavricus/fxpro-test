import { Validator } from 'class-validator';
import { FxproValidationError, FxproValidationErrorDetails } from '../../../errors';
import { CreatePaymentRequest } from './service';

export interface PaymentCreateRequestBuilder {
    build(data: CreatePaymentRequest): string;
}

export class PaymentCreateRequestBuilderImpl implements PaymentCreateRequestBuilder {
    constructor (private validator: PaymentCreateRequestValidator, private formatter: PaymentCreateRequestFormatter) {
    }

    build(data: CreatePaymentRequest): string {
        this.validator.validate(data);
        return this.formatter.format(data);
    }
}

export interface PaymentCreateRequestFormatter {
    format(data: CreatePaymentRequest): string;
}

export class PaymentCreateRequestFormatterImpl implements PaymentCreateRequestFormatter {
    format(data: CreatePaymentRequest): string {
        return JSON.stringify(data);
    }

}

export interface PaymentCreateRequestValidator {
    validate(data: CreatePaymentRequest): void;
}

export class PaymentCreateRequestValidatorImpl implements PaymentCreateRequestValidator {
    private validator: Validator = new Validator();

    validate(data: CreatePaymentRequest): void {
        let path: Array<string> = [];

        let details: Array<FxproValidationErrorDetails<unknown>> = (<Array<FxproValidationErrorDetails<unknown>>>[]).concat(
            this.validatePayeeId(data, path),
            this.validatePayerId(data, path),
            this.validatePaymentSystem(data, path),
            this.validatePaymentMethod(data, path),
            this.validateAmount(data, path),
            this.validateCurrency(data, path),
            this.validateComment(data, path));

        if (details.length) {
            throw new FxproValidationError(details);
        }
        return;
    }

    private validateComment(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<unknown>> {
        let name: keyof CreatePaymentRequest = 'currency';
        let value = data[name];
        let details: Array<FxproValidationErrorDetails<string>> = [];

        if (this.validator.isEmpty(value)) { //Optional field
            return details;
        }

        if (!this.validator.isString(value)) {
            details.push(this.getDetails('Value must be a string', path, name, value));
        }
        
        return details;
    }

    private validateCurrency(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<string>> {
        let name: keyof CreatePaymentRequest = 'currency';
        let value = data[name];

        let details: Array<FxproValidationErrorDetails<string>> = [];
        if (!this.validator.isString(value)) {
            details.push(this.getDetails('Value must be a string', path, name, value));
        }
        if (!this.validator.isIn(value, ['RUR', 'EUR', 'USD'])) { //TODO: Must be taken from the CurrencyProvider.getAll(). Possible future check for currency based on the Payment System.
            details.push(this.getDetails('Unsupported currency', path, name, value));
        }
        return details;
    }

    private validatePaymentSystem(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<string>> {
        let name: keyof CreatePaymentRequest = 'paymentSystem';
        let value = data[name];

        let details: Array<FxproValidationErrorDetails<string>> = [];
        if (!this.validator.isString(value)) {
            details.push(this.getDetails('Value must be a string', path, name, value));
        }
        if (!this.validator.isIn(value, ['payPal', 'yandexMoney', 'ingenico'])) { //TODO: Must be taken from the PaymentSystemProvider.getAll()
            details.push(this.getDetails('Unsupported payment system', path, name, value));
        }
        return details;
    }

    private validatePaymentMethod(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<string>> {
        let name: keyof CreatePaymentRequest = 'paymentMethod';
        let value = data[name];

        let details: Array<FxproValidationErrorDetails<string>> = [];
        if (!this.validator.isString(value)) {
            details.push(this.getDetails('Value must be a string', path, name, value));
        }
        if (!this.validator.isIn(value, ['PBM', 'visa', 'mastercard'])) { //TODO: Must be taken from the PaymentMethodProvider.getAll(). Possible future check for payment method based on the Payment System.
            details.push(this.getDetails('Unsupported payment method', path, name, value));
        }
        return details;
    }

    private validatePayerId(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<string>> {
        let name: keyof CreatePaymentRequest = 'payerId';
        return this.validateUUID(data[name], path, name);
    }

    private validatePayeeId(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<string>> {
        let name: keyof CreatePaymentRequest = 'payeeId';
        return this.validateUUID(data[name], path, name);
    }

    private validateUUID(value: string, path: Array<string>, name: string): Array<FxproValidationErrorDetails<string>> {
        let details: Array<FxproValidationErrorDetails<string>> = [];
        if (!this.validator.isString(value)) {
            details.push(this.getDetails('Value must be a string', path, name, value));
        }
        if (!this.validator.isUUID(value)) {
            details.push(this.getDetails('Value has invalid format', path, name, value));
        }
        return details;
    }

    private getDetails<T>(descriprion: string, path: Array<string>, name: string, value: T): FxproValidationErrorDetails<T> {
        return {
            message: descriprion,
            path: path.concat([name]),
            value
        };
    }

    private validateAmount(data: CreatePaymentRequest, path: Array<string>): Array<FxproValidationErrorDetails<number>> {
        let name: keyof CreatePaymentRequest = 'amount';
        let value = data[name];

        let details: Array<FxproValidationErrorDetails<number>> = [];
        if (!this.validator.isNumber(value)) {
            details.push(this.getDetails('Value must be a number', path, name, value));
        }
        if (!this.validator.min(value, 0)) {
            details.push(this.getDetails('Value must be grater then 0', path, name, value));
        }

        return details;
    }
}