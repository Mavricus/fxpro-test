import * as url from 'url';
import { PaymentCreateRequestBuilder } from './create_payment_request_builder';
import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';
import { CODES, FxproError } from '../../../errors';
import { ServerConfig } from '../../../factory';
import { PaymentInfo } from '../types';
import { BaseRequestWithData } from '../../types';

export interface CreatePaymentRequest {
    payeeId: string;
    payerId: string;
    paymentSystem: string;
    paymentMethod: string;
    amount: number;
    currency: string;
    comment?: string | null;
}

export interface CreatePaymentService {
    create(data: BaseRequestWithData<CreatePaymentRequest>): Promise<PaymentInfo>;
}

export class CreatePaymentServiceImpl implements CreatePaymentService {
    private static readonly POSITIVE_RESPONSE: number = 201;
    private readonly url: string;

    constructor(private config: ServerConfig, private requestBuilder: PaymentCreateRequestBuilder) {
        this.url = url.format({
            protocol: config.protocol,
            hostname: config.hostname,
            port: config.port,
            pathname: '/v1/payments'
        });
    }

    async create(data: BaseRequestWithData<CreatePaymentRequest>): Promise<PaymentInfo> {
        try {
            const reqBody = this.requestBuilder.build(data.data);
            const config = CreatePaymentServiceImpl.buildConfig(data);
            const resp: AxiosResponse<PaymentInfo> = await axios.post(this.url, reqBody, config);
            if (resp.status !== CreatePaymentServiceImpl.POSITIVE_RESPONSE) {
                if (resp.status === 400) {
                    throw new FxproError(CODES.validation, 'Invalid request');
                }
                if (resp.status === 401) {
                    throw new FxproError(CODES.tokenExpired, 'Auth token expired');
                }
                throw new FxproError(CODES.cannotCreate, 'Cannot create new payment');
            }

            return resp.data;
            
        } catch (e) {
            if (e.code != null) {
                throw e;
            }
            throw new FxproError(CODES.cannotCreate, e.message);
        }
    }

    private static buildConfig(data: BaseRequestWithData<CreatePaymentRequest>): AxiosRequestConfig {
        if(data.token == null) {
            throw new FxproError(CODES.unauthorized, 'Token is missed');
        }
        return {
            headers: {
                token: data.token
            }
        };
    }
}