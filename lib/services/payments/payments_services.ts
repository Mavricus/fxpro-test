import { CreatePaymentRequest, CreatePaymentService } from './create/service';
import { GetAllPaymentsService } from './get_all/service';
import { PaymentInfo } from './types';
import { GetPaymentByIdService, PaymentServiceByIdRequest } from './get_by_id/service';
import { BaseRequest, BaseRequestWithData } from '../types';

export interface PaymentServices extends CreatePaymentService, GetAllPaymentsService, GetPaymentByIdService {
}

export class PaymentServicesImpl implements PaymentServices {
    constructor(private createService: CreatePaymentService, private getAllPaymentsService: GetAllPaymentsService,
                private getPaymentByIdService: GetPaymentByIdService) {
    }

    create(data: BaseRequestWithData<CreatePaymentRequest>): Promise<PaymentInfo> {
        return this.createService.create(data);
    }

    getAll(data: BaseRequest): Promise<Array<PaymentInfo>> {
        return this.getAllPaymentsService.getAll(data);
    }

    getById(data: BaseRequestWithData<PaymentServiceByIdRequest>): Promise<PaymentInfo> {
        return this.getPaymentByIdService.getById(data);
    }
}