import * as url from 'url';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { CODES, FxproError } from '../../../errors';
import { ServerConfig } from '../../../factory';
import { BaseRequestWithData } from '../../types';
import { PaymentInfo } from '../types';

export interface PaymentServiceByIdRequest {
    paymentId: string;
}

export interface GetPaymentByIdService {
    getById(data: BaseRequestWithData<PaymentServiceByIdRequest>): Promise<PaymentInfo>;
}

export class GetPaymentByIdServiceImpl implements GetPaymentByIdService {
    private static readonly POSITIVE_RESPONSE = 200;
    private readonly url: string;

    constructor(private config: ServerConfig) {
        this.url = url.format({
            protocol: config.protocol,
            hostname: config.hostname,
            port: config.port,
            pathname: '/v1/payment/{paymentId}'
        });
    }

    async getById(data: BaseRequestWithData<PaymentServiceByIdRequest>): Promise<PaymentInfo> {
        try {
            const config = GetPaymentByIdServiceImpl.buildConfig(data);
            const url = this.buildUrl(this.url, data.data);
            const resp: AxiosResponse<PaymentInfo> = await axios.get(url, config);
            if (resp.status !== GetPaymentByIdServiceImpl.POSITIVE_RESPONSE) {
                if (resp.status === 400) {
                    throw new FxproError(CODES.validation, 'Invalid request');
                }
                if (resp.status === 401) {
                    throw new FxproError(CODES.tokenExpired, 'Auth token expired');
                }
                throw new FxproError(CODES.cannotGet, 'Cannot create new payment');
            }
            return resp.data;

        } catch (e) {
            if (e.code != null) {
                throw e;
            }
            if (e.response && e.response.status === 404) {
                throw new FxproError(CODES.validation, 'Payment cannot be found');
            }
            throw new FxproError(CODES.cannotGet, e.message);
        }
    }

    private static buildConfig(data: BaseRequestWithData<PaymentServiceByIdRequest>): AxiosRequestConfig {
        if(data.token == null) {
            throw new FxproError(CODES.unauthorized, 'Token is missed');
        }
        return {
            headers: {
                token: data.token
            }
        };
    }

    private buildUrl(url: string, data: PaymentServiceByIdRequest): string {
        return url.replace('{paymentId}', data.paymentId);
    }
}