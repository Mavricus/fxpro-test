export interface PaymentInfo {
    id: string;
    payeeId: string;
    payerId: string;
    paymentSystem: string;
    paymentMethod: string;
    amount: number;
    currency: string;
    status: string;
    comment: string;
    created: string;
    updated: string;
}