import * as url from 'url';
import axios, { AxiosResponse } from 'axios';
import { CODES, FxproError } from '../../../errors';
import { ServerConfig } from '../../../factory';
import { PaymentInfo } from '../types';
import { BaseRequest } from '../../types';


export interface GetAllPaymentsService {
    getAll(data: BaseRequest): Promise<Array<PaymentInfo>>;
}

export class GetAllPaymentsServiceImpl implements GetAllPaymentsService {
    private readonly url: string;
    private static readonly POSITIVE_RESPONSE = 200;

    
    constructor(config: ServerConfig) {
        this.url = url.format({
            protocol: config.protocol,
            hostname: config.hostname,
            port: config.port,
            pathname: '/v1/payments'
        });
    }

    async getAll(data: BaseRequest): Promise<Array<PaymentInfo>> {
        try {
            const config = GetAllPaymentsServiceImpl.buildConfig(data);
            const resp: AxiosResponse<Array<PaymentInfo>> = await axios.get(this.url, config);
            if (resp.status !== GetAllPaymentsServiceImpl.POSITIVE_RESPONSE) {
                if (resp.status === 400) {
                    throw new FxproError(CODES.validation, 'Invalid request');
                }
                if (resp.status === 401) {
                    throw new FxproError(CODES.tokenExpired, 'Auth token expired');
                }
                throw new FxproError(CODES.cannotGet, 'Cannot get payments list');
            }

            return resp.data;

        } catch (e) {
            if (e.code != null) {
                throw e;
            }
            throw new FxproError(CODES.cannotGet, e.message);
        }
    }

    private static buildConfig(data: BaseRequest) {
        if(data.token == null) {
            throw new FxproError(CODES.unauthorized, 'Token is missed');
        }
        return {
            headers: {
                token: data.token
            }
        };
    }
}