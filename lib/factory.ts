import { CreatePaymentServiceImpl } from './services/payments/create/service';
import {
    PaymentCreateRequestBuilderImpl, PaymentCreateRequestFormatterImpl, PaymentCreateRequestValidatorImpl
} from './services/payments/create/create_payment_request_builder';
import { GetAllPaymentsServiceImpl } from './services/payments/get_all/service';
import { PaymentServices, PaymentServicesImpl } from './services/payments/payments_services';
import { GetPaymentByIdServiceImpl } from './services/payments/get_by_id/service';
import { TokenProviderService, TokenProviderImpl } from './services/authenticate/token_provider';


export interface ServerConfig {
    protocol: string;
    hostname: string;
    port: number;
}

export let tokenProvider = (config: ServerConfig): TokenProviderService => {
    return new TokenProviderImpl(config);
};

export let paymentServices = (config: ServerConfig): PaymentServices => {
    let paymentCreateRequestValidator =  new PaymentCreateRequestValidatorImpl();
    let paymentCreateRequestFormatter = new PaymentCreateRequestFormatterImpl();
    let paymentCreateRequestBuilder = new PaymentCreateRequestBuilderImpl(paymentCreateRequestValidator, paymentCreateRequestFormatter);
    let createPaymentService = new CreatePaymentServiceImpl(config, paymentCreateRequestBuilder);

    let getAllPaymentsService = new GetAllPaymentsServiceImpl(config);

    let getPaymentByIdService = new GetPaymentByIdServiceImpl(config);

    return new PaymentServicesImpl(createPaymentService, getAllPaymentsService, getPaymentByIdService);
};