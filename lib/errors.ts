export const CODES = Object.freeze({
    unauthorized: 'ERR_UNATHORIZED',  // WARN: spelling error at the API description
    tokenExpired: 'ERR_AUTH_TOKEN_EXPIRED',
    validation: 'ERR_VALIDATION',
    cannotApprove: 'ERR_CANNOT_APPROVE',
    cannotCancel: 'ERR_CANNOT_CANCEL',
    cannotCreate: 'ERR_CANNOT_CREATE',
    cannotGet: 'ERR_CANNOT_GET',
});

export class FxproError extends Error {
    name = 'FxproError';
    code: string;
        
    constructor(code: string, message?: string) {
        super(message);
        
        this.code = code;
        this.message = message || 'An error occurred during the request processing';
        (<any>Error).captureStackTrace(this, FxproError);
    }
}

export interface FxproValidationErrorDetails<T> {
    message: string;
    path: Array<string>;
    value: T;
}

export class FxproValidationError extends FxproError {
    name = 'FxproValidationError';
    details: Array<FxproValidationErrorDetails<unknown>>;

    constructor(details: Array<FxproValidationErrorDetails<any>>, message?: string) {
        super(CODES.validation, message);

        this.details = details;
        this.message = message || 'Validation failed';
        (<any>Error).captureStackTrace(this, FxproValidationError);
    }
}   