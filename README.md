## FxPro Test Lib 

This is the test lib for the FxPro interview.

## Usage

Below is a simple use case of the lib.

```javascript
const { paymentServices, tokenProvider } = require('fxprotest');
const config = {
    protocol: 'http',
    hostname: '127.0.0.1',
    port: 7357
};

const authInfo = { username: 'serious_business', password: 'Test!@#' };

const newPaymentInfo = {
    payeeId: 'fc1941f3-7912-4b3d-8fdb-dcb9733aa994',
    payerId: '0499274e-9325-43b1-9cff-57c957e9a337',
    paymentSystem: 'ingenico',
    paymentMethod: 'mastercard',
    amount: 0,
    currency: 'USD',
    comment: 'Salary for March'
};

const tp = tokenProvider(config);
const ps = paymentServices(config);

let token;

tp.get(authInfo)                                                // Getting auth token
  .then(({ authToken }) => {
      token = authToken;
      return ps.create({ token, data: newPaymentInfo});         // Creation of the new payment
  }).then(({ id }) => {
      return ps.getById({ token, data: { paymentId: id } });    // Getting details of new payment
  }).then((paymentInfo) => {
      console.log(`Payment id: \t${paymentInfo.id}`);
      console.log(`Payee id: \t${paymentInfo.payeeId}`);
      console.log(`Payer id: \t${paymentInfo.payerId}`);
      console.log(`Amount id: \t${paymentInfo.amount}`);
  }).catch(err => {
      console.log(`Code: \t${err.code}`);
      console.log(`Error: \t${err.stack}`);
  });
```
