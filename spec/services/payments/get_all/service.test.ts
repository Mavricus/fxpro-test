import { GetAllPaymentsServiceImpl } from '../../../../lib/services/payments/get_all/service';
import axios, { AxiosResponse } from 'axios';
import { ServerConfig } from '../../../../lib/factory';
import { PaymentInfo } from '../../../../lib/services/payments/types';
import { CODES, FxproError } from '../../../../lib/errors';
import { BaseRequest } from '../../../../lib/services/types';

jest.mock('axios');

describe('GetAllPaymentsServiceImpl', () => {
    let service: GetAllPaymentsServiceImpl;
    let response: Array<PaymentInfo>;
    let request: BaseRequest;
    let axiosResult: AxiosResponse<Array<PaymentInfo>>;

    let mockedAxios: jest.Mocked<typeof axios>;

    beforeEach(() => {
        let config: ServerConfig = {
            protocol: 'https',
            hostname: 'test.ts',
            port: 123
        };

        response = [{
            id: 'Test id',
            payeeId: 'Test payeeId',
            payerId: 'Test payerId',
            paymentSystem: 'Test paymentSystem',
            paymentMethod: 'Test paymentMethod',
            amount: 100500.12,
            currency: 'Test currency',
            status: 'Test status',
            comment: 'Test comment',
            created: 'Test created',
            updated: 'Test updated'
        }];

        axiosResult = {
            data: response,
            status: 200,
            headers: [],
            request: null,
            config: {},
            statusText: 'Test status'
        };

        request = {
            token: 'test Token'
        };

        mockedAxios = axios as jest.Mocked<typeof axios>;
        mockedAxios.get.mockResolvedValue(axiosResult);

        service = new GetAllPaymentsServiceImpl(config);
    });

    describe('get', () => {
        it('should return the list of all payments', () => {
            return service.getAll(request).then((result) => {
                expect(result).toBe(response);
            })
        });
        it('should return aт unauthorized error if token is missed', () => {
            delete request.token;
            return service.getAll(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.unauthorized);
            });
        });
        it('should return a validation error if status code is not 400', () => {
            axiosResult.status = 400;
            return service.getAll(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.validation);
            });
        });
        it('should return a token expired error if status code is not 401', () => {
            axiosResult.status = 401;
            return service.getAll(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.tokenExpired);
            });
        });
        it('should return getting error if status code is not 200', () => {
            axiosResult.status = 201;
            return service.getAll(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotGet);
            });
        });
        it('should cast error to getting FxproError if some error occurred', () => {
            let message = 'Test error message';
            mockedAxios.get.mockRejectedValue(new Error(message));
            return service.getAll(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotGet);
                expect(e.message).toBe(message);
            });
        });
        it('should build correct URL', () => {
            return service.getAll(request).then(() => {
                expect(mockedAxios.get.mock.calls[0][0]).toBe('https://test.ts:123/v1/payments');
            });
        });
    });
});
