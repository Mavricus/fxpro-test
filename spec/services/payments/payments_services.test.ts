import { PaymentServices, PaymentServicesImpl } from '../../../lib/services/payments/payments_services';
import { GetAllPaymentsService, GetAllPaymentsServiceImpl } from '../../../lib/services/payments/get_all/service';
import { GetPaymentByIdService, PaymentServiceByIdRequest } from '../../../lib/services/payments/get_by_id/service';
import { CreatePaymentRequest, CreatePaymentService } from '../../../lib/services/payments/create/service';
import { PaymentInfo } from '../../../lib/services/payments/types';
import { BaseRequest, BaseRequestWithData } from '../../../lib/services/types';
import { SinonStatic, SinonStub } from 'sinon';
import sinon from 'sinon';

class GetAllPaymentsServiceStub implements GetAllPaymentsService {
    stubs: {
        getAll: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            getAll: sinon.stub()
        }
    }

    getAll(data: BaseRequest): Promise<Array<PaymentInfo>> {
        return this.stubs.getAll(data);
    }
}


class GetPaymentByIdServiceStub implements GetPaymentByIdService {
    stubs: {
        getById: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            getById: sinon.stub()
        }
    }

    getById(data: BaseRequestWithData<PaymentServiceByIdRequest>): Promise<PaymentInfo> {
        return this.stubs.getById(data);
    }
}


class CreatePaymentServiceStub implements CreatePaymentService {
    stubs: {
        create: SinonStub;
    };
    
    constructor(sinon: SinonStatic) {
        this.stubs = {
            create: sinon.stub()
        }
    }

    create(data: BaseRequestWithData<CreatePaymentRequest>): Promise<PaymentInfo> {
        return this.stubs.create(data);
    }
}

describe('PaymentServicesImpl', () => {
    let service: PaymentServicesImpl;
    let getAllService: GetAllPaymentsServiceStub;
    let getByIdService: GetPaymentByIdServiceStub;
    let createService: CreatePaymentServiceStub;
    let getAllResult: string;
    let getByIdResult: string;
    let createResult: string;

    beforeEach(() => {
        getAllResult = 'getAllResult';
        getByIdResult = 'getByIdResult';
        createResult = 'createResult';

        getAllService = new GetAllPaymentsServiceStub(sinon);
        getAllService.stubs.getAll.returns(Promise.resolve(getAllResult));

        getByIdService = new GetPaymentByIdServiceStub(sinon);
        getByIdService.stubs.getById.returns(Promise.resolve(getByIdResult));

        createService = new CreatePaymentServiceStub(sinon);
        createService.stubs.create.returns(Promise.resolve(createResult));

        service = new PaymentServicesImpl(createService, getAllService, getByIdService);
    });

    describe('create', () => {
        it('should proxy create request to the create service and return the same result', ()=> {
            return service.create(<BaseRequestWithData<CreatePaymentRequest>>{}).then((result) => {
                expect(result).toBe(createResult);
            });
        });
        
        it('should return the same error that create service returns', ()=> {
            let exception = new Error('Test error');
            createService.stubs.create.returns(Promise.reject(exception));
            return service.create(<BaseRequestWithData<CreatePaymentRequest>>{}).then((result) => {
                expect(result).not.toBe(createResult);
            }).catch((err)=> {
                expect(err).toBe(exception);
            });
        });
    });

    describe('getById', () => {
        it('should proxy getById request to the create service and return the same result', ()=> {
            return service.getById(<BaseRequestWithData<PaymentServiceByIdRequest>>{}).then((result) => {
                expect(result).toBe(getByIdResult);
            });
        });

        it('should return the same error that getById service returns', ()=> {
            let exception = new Error('Test error');
            getByIdService.stubs.getById.returns(Promise.reject(exception));
            return service.getById(<BaseRequestWithData<PaymentServiceByIdRequest>>{}).then((result) => {
                expect(result).not.toBe(getByIdResult);
            }).catch((err)=> {
                expect(err).toBe(exception);
            });
        });
    });

    describe('getAll', () => {
        it('should proxy getAll request to the create service and return the same result', ()=> {
            return service.getAll(<BaseRequest>{}).then((result) => {
                expect(result).toBe(getAllResult);
            });
        });
        
        it('should return the same error that getAll service returns', ()=> {
            let exception = new Error('Test error');
            getAllService.stubs.getAll.returns(Promise.reject(exception));
            return service.getAll(<BaseRequest>{}).then((result) => {
                expect(result).not.toBe(getAllResult);
            }).catch((err)=> {
                expect(err).toBe(exception);
            });
        });
    });
});