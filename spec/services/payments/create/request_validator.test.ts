import { PaymentCreateRequestValidatorImpl } from '../../../../lib/services/payments/create/create_payment_request_builder';
import { CreatePaymentRequest } from '../../../../lib/services/payments/create/service';

describe('PaymentCreateRequestValidatorImpl', () => {
    let validator: PaymentCreateRequestValidatorImpl;
    let request: CreatePaymentRequest;

    beforeEach(() => {
        request = {
            payeeId: 'fc1941f3-7912-4b3d-8fdb-dcb9733aa994',
            payerId: '0499274e-9325-43b1-9cff-57c957e9a337',
            paymentSystem: 'ingenico',
            paymentMethod: 'mastercard',
            amount: 0,
            currency: 'USD',
            comment: 'Salary for March'
        };

        validator = new PaymentCreateRequestValidatorImpl();
    });

    describe('validate', () => {
        it('should not throw error if the message is valid', () => {
            expect(() => validator.validate(request)).not.toThrow();
        });

        it('should throw error if payeeId is not UUID formatted', () => {
            request.payeeId = 'test';
            expect(() => validator.validate(request)).toThrow();
        });

        it('should throw error if payerId is not UUID formatted', () => {
            request.payerId = 'test';
            expect(() => validator.validate(request)).toThrow();
        });

        it('should throw error if paymentSystem is not in list of available systems', () => {
            request.paymentSystem = 'test';
            expect(() => validator.validate(request)).toThrow();
        });

        it('should throw error if paymentMethod is not in list of available methods', () => {
            request.paymentMethod = 'test';
            expect(() => validator.validate(request)).toThrow();
        });

        it('should throw error if amount is less then 0', () => {
            request.amount = -5;
            expect(() => validator.validate(request)).toThrow();
        });

        it('should not throw error if amount is 0', () => {
            request.amount = 0;
            expect(() => validator.validate(request)).not.toThrow();
        });

        it('should throw error if currency is not in list of available currencies', () => {
            request.paymentMethod = 'test';
            expect(() => validator.validate(request)).toThrow();
        });

        it('should not throw error if comment is empty', () => {
            request.comment = '';
            expect(() => validator.validate(request)).not.toThrow();
        });

        it('should not throw error if comment is null', () => {
            request.comment = null;
            expect(() => validator.validate(request)).not.toThrow();
        });

        it('should not throw error if comment is absent', () => {
            delete request.comment;
            expect(() => validator.validate(request)).not.toThrow();
        });
    })
});