import {
    PaymentCreateRequestBuilderImpl, PaymentCreateRequestFormatter, PaymentCreateRequestValidator
} from '../../../../lib/services/payments/create/create_payment_request_builder';
import { CreatePaymentRequest } from '../../../../lib/services/payments/create/service';
import { SinonStatic, SinonStub } from 'sinon';
import sinon from 'sinon';

class ValidatorStub implements PaymentCreateRequestValidator {
    stubs: {
        validate: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            validate: sinon.stub()
        };
    }

    validate(data: CreatePaymentRequest): void {
        return this.stubs.validate(data);
    }
}

class FormatterStub implements PaymentCreateRequestFormatter {
    stubs: {
        format: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            format: sinon.stub()
        };
    }

    format(data: CreatePaymentRequest): string {
        return this.stubs.format(data);
    }
}

describe('PaymentCreateRequestBuilderImpl', () => {
    let builder: PaymentCreateRequestBuilderImpl;
    let request: CreatePaymentRequest;
    let validator: ValidatorStub;
    let formatter: FormatterStub;
    let formattedData: string;

    beforeEach(() => {
        formattedData = 'Formatted Data';

        request = {
            payeeId: 'Test payeeId',
            payerId: 'Test payerId',
            paymentSystem: 'Test paymentSystem',
            paymentMethod: 'Test paymentMethod',
            amount: 123,
            currency: 'Test currency',
            comment: 'Test comment'
        };

        validator = new ValidatorStub(sinon);
        validator.stubs.validate.returns(null);

        formatter = new FormatterStub(sinon);
        formatter.stubs.format.returns(formattedData);

        builder = new PaymentCreateRequestBuilderImpl(validator, formatter);
    });

    describe('build', () => {
        it('should validate and build request message', () => {
            let result = builder.build(request);
            expect(result).toBe(formattedData);
        });

        it('should validate message and throw error which validator threw', () => {
            let err = new Error('Test error');
            validator.stubs.validate.throws(err);
            expect(() => builder.build(request)).toThrow(err);
        });

        it('should proxy the error which is thrown by formatter', () => {
            let err = new Error('Test error');
            formatter.stubs.format.throws(err);
            expect(() => builder.build(request)).toThrow(err);
        });
    });
});