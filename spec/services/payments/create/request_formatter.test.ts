import { PaymentCreateRequestFormatterImpl } from '../../../../lib/services/payments/create/create_payment_request_builder';
import { CreatePaymentRequest } from '../../../../lib/services/payments/create/service';

describe('', () => {
    let formatter: PaymentCreateRequestFormatterImpl;
    let request: CreatePaymentRequest;

    beforeEach(() => {
        request = {
            payeeId: 'Test payeeId',
            payerId: 'Test payerId',
            paymentSystem: 'Test paymentSystem',
            paymentMethod: 'Test paymentMethod',
            amount: 123,
            currency: 'Test currency',
            comment: 'Test comment'
        };
        formatter = new PaymentCreateRequestFormatterImpl();
    });

    describe('format', () => {
        it('should return the JSON string based on the passed object', () => {
            let data = formatter.format(request);
            expect(JSON.parse(data)).toEqual(request);
        });
    });
});