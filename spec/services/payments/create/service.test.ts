import axios, { AxiosResponse } from 'axios';
import { ServerConfig } from '../../../../lib/factory';
import { PaymentInfo } from '../../../../lib/services/payments/types';
import { BaseRequestWithData } from '../../../../lib/services/types';
import { CODES, FxproError } from '../../../../lib/errors';
import { CreatePaymentRequest, CreatePaymentServiceImpl } from '../../../../lib/services/payments/create/service';
import { PaymentCreateRequestBuilder } from '../../../../lib/services/payments/create/create_payment_request_builder';
import { SinonStatic, SinonStub } from 'sinon';
import sinon from 'sinon';

jest.mock('axios');

class PaymentCreateRequestBuilderStub implements PaymentCreateRequestBuilder {
    stubs: {
        build: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            build: sinon.stub()
        };
    }

    build(data: CreatePaymentRequest): string {
        return this.stubs.build(data);
    }
}

describe('CreatePaymentServiceImpl', () => {
    let service: CreatePaymentServiceImpl;
    let response: PaymentInfo;
    let request: BaseRequestWithData<CreatePaymentRequest>;
    let builder: PaymentCreateRequestBuilderStub;
    let axiosResult: AxiosResponse<PaymentInfo>;
    let requestData: string;

    let mockedAxios: jest.Mocked<typeof axios>;

    beforeEach(() => {
        let config: ServerConfig = {
            protocol: 'https',
            hostname: 'test.ts',
            port: 123
        };

        response = {
            id: 'Test id',
            payeeId: 'Test payeeId',
            payerId: 'Test payerId',
            paymentSystem: 'Test paymentSystem',
            paymentMethod: 'Test paymentMethod',
            amount: 100500.12,
            currency: 'Test currency',
            status: 'Test status',
            comment: 'Test comment',
            created: 'Test created',
            updated: 'Test updated'
        };

        axiosResult = {
            data: response,
            status: 201,
            headers: [],
            request: null,
            config: {},
            statusText: 'Test status'
        };

        request = {
            token: 'test Token',
            data: {
                payeeId: 'payeeId',
                payerId: 'payerId',
                paymentSystem: 'paymentSystem',
                paymentMethod: 'paymentMethod',
                amount: 123.00,
                currency: 'currency',
                comment: 'comment'
            }
        };

        requestData = 'Test requestData';

        mockedAxios = axios as jest.Mocked<typeof axios>;
        mockedAxios.post.mockResolvedValue(axiosResult);

        builder = new PaymentCreateRequestBuilderStub(sinon);
        builder.stubs.build.returns(requestData);

        service = new CreatePaymentServiceImpl(config, builder);
    });

    describe('create', () => {
        it('should return the new payment details', () => {
            return service.create(request).then((result) => {
                expect(result).toBe(response);
            })
        });
        it('should return an unauthorized error if token is missed', () => {
            delete request.token;
            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.unauthorized);
            });
        });
        it('should return a validation error if token is missed', () => {
            axiosResult.status = 400;
            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.validation);
            });
        });
        it('should return a token expired error if status code is not 401', () => {
            axiosResult.status = 401;
            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.tokenExpired);
            });
        });
        it('should return getting error if status code is not 201', () => {
            axiosResult.status = 200;
            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotCreate);
            });
        });
        it('should cast error to creation FxproError if some error occurred', () => {
            let message = 'Test error message';
            mockedAxios.post.mockRejectedValue(new Error(message));
            
            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotCreate);
                expect(e.message).toBe(message);
            });
        });
        it('should proxy FxproErrors', () => {
            let exception = new FxproError(CODES.validation, 'Test error message');
            builder.stubs.build.throws(exception);

            return service.create(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e).toBe(exception);
            });
        });
        it('should build correct URL', () => {
            return service.create(request).then(() => {
                expect(mockedAxios.post.mock.calls[0][0]).toBe('https://test.ts:123/v1/payments');
            });
        });
        it('should post the request body which is build by RequestBuilder', () => {
            return service.create(request).then(() => {
                expect(mockedAxios.post.mock.calls[0][1]).toBe(requestData);
            });
        });
    });
});
