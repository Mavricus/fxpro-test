import axios, { AxiosResponse } from 'axios';
import { GetPaymentByIdServiceImpl, PaymentServiceByIdRequest } from '../../../../lib/services/payments/get_by_id/service';
import { ServerConfig } from '../../../../lib/factory';
import { PaymentInfo } from '../../../../lib/services/payments/types';
import { BaseRequestWithData } from '../../../../lib/services/types';
import { CODES, FxproError } from '../../../../lib/errors';

jest.mock('axios');

describe('GetPaymentByIdServiceImpl', () => {
    let service: GetPaymentByIdServiceImpl;
    let response: PaymentInfo;
    let request: BaseRequestWithData<PaymentServiceByIdRequest>;
    let axiosResult: AxiosResponse<PaymentInfo>;

    let mockedAxios: jest.Mocked<typeof axios>;

    beforeEach(() => {
        let config: ServerConfig = {
            protocol: 'https',
            hostname: 'test.ts',
            port: 123
        };

        response = {
            id: 'Test id',
            payeeId: 'Test payeeId',
            payerId: 'Test payerId',
            paymentSystem: 'Test paymentSystem',
            paymentMethod: 'Test paymentMethod',
            amount: 100500.12,
            currency: 'Test currency',
            status: 'Test status',
            comment: 'Test comment',
            created: 'Test created',
            updated: 'Test updated'
        };

        axiosResult = {
            data: response,
            status: 200,
            headers: [],
            request: null,
            config: {},
            statusText: 'Test status'
        };

        request = {
            token: 'test Token',
            data: {
                paymentId: 'TestPaymentId'
            }
        };

        mockedAxios = axios as jest.Mocked<typeof axios>;
        mockedAxios.get.mockResolvedValue(axiosResult);

        service = new GetPaymentByIdServiceImpl(config);
    });

    describe('get', () => {
        it('should return the payment with the requested id', () => {
            return service.getById(request).then((result) => {
                expect(result).toBe(response);
            })
        });
        it('should return an unauthorized error if token is missed', () => {
            delete request.token;
            return service.getById(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.unauthorized);
            });
        });
        it('should return a validation error if token is missed', () => {
            axiosResult.status = 400;
            return service.getById(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.validation);
            });
        });
        it('should return a token expired error if status code is not 401', () => {
            axiosResult.status = 401;
            return service.getById(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.tokenExpired);
            });
        });
        it('should return getting error if status code is not 200', () => {
            axiosResult.status = 201;
            return service.getById(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotGet);
            });
        });
        it('should cast error to getting FxproError if some error occurred', () => {
            let message = 'Test error message';
            mockedAxios.get.mockRejectedValue(new Error(message));
            return service.getById(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.cannotGet);
                expect(e.message).toBe(message);
            });
        });
        it('should build correct URL', () => {
            return service.getById(request).then(() => {
                expect(mockedAxios.get.mock.calls[0][0]).toBe('https://test.ts:123/v1/payment/TestPaymentId');
            });
        });
    });
});
