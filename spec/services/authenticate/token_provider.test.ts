import { AuthenticationRequest, AuthenticationResponse, TokenProviderImpl } from '../../../lib/services/authenticate/token_provider';
import { ServerConfig } from '../../../lib/factory';
import axios, { AxiosResponse } from 'axios';
import { CODES, FxproError } from '../../../lib/errors';

jest.mock('axios');

describe('TokenProviderImpl', () => {
    let provider: TokenProviderImpl;
    let request: AuthenticationRequest;
    let response: AuthenticationResponse;
    let axiosResult: AxiosResponse<AuthenticationResponse>;
    let mockedAxios: jest.Mocked<typeof axios>;

    beforeEach(() => {
        let config: ServerConfig = {
            protocol: 'https',
            hostname: 'test.ts',
            port: 777
        };

        request = {
            username: 'tester',
            password: 'te$t'
        };

        response = {
            authToken: 'My token',
            expiresIn: 'Exp date'
        };

        axiosResult = {
            data: response,
            status: 200,
            headers: [],
            request: request,
            config: {},
            statusText: 'Test status'
        };

        mockedAxios = axios as jest.Mocked<typeof axios>;
        mockedAxios.post.mockResolvedValue(axiosResult);

        provider = new TokenProviderImpl(config);
    });

    describe('get', () => {
        it('should return the new token', () => {
            return provider.get(request).then((result) => {
                expect(result).toBe(response);
            })
        });
        it('should return the new token if response has code 201', () => {
            axiosResult.status = 201;
            return provider.get(request).then((result) => {
                expect(result).toBe(response);
            });
        });
        it('should return an unauthorized error if  status code is not 200 or 201', () => {
            axiosResult.status = 400;
            return provider.get(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.unauthorized);
            });
        });
        it('should return an authorized error if some error occurred', () => {
            let message = 'Test error message';
            mockedAxios.post.mockRejectedValue(new Error(message));
            return provider.get(request).then((result) => {
                expect(result).not.toBe(response);
            }).catch((e: FxproError) => {
                expect(e.code).toBe(CODES.unauthorized);
                expect(e.message).toBe(message);
            });
        });
        it('should build correct URL', () => {
            return provider.get(request).then((result) => {
                expect(mockedAxios.post.mock.calls[0][0]).toBe('https://test.ts:777/v1/authenticate');
            });
        });
    });
});